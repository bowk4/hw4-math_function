function calculate(num1, num2, operator) {
    switch (operator) {
        case "+":
            return num1 + num2;
            break;
        case "-":
            return num1 - num2;
            break;
        case "*":
            return num1 * num2;
            break;
        case "/":
            return num1 / num2;
            break;
        default:
            alert("I don't know this operation");
    }
}


let number1 = parseInt(prompt("Enter first number:"));
let number2 = parseInt(prompt("Enter second number:"));

while (isNaN(number1) || isNaN(number2) && (number1 !== null && number2 !== null)) {
    number1 = parseInt(prompt("Please, repeat First number:", number1));
    number2 = parseInt(prompt("Please, repeat Second number:", number2));
}

let operator = prompt("Select an operation ('+', '-', '*', '/',):");

let result = calculate(number1, number2, operator);
console.log(`---====--- Result: ${result} ---====---`);

